const puppeteer = require('puppeteer');
const expect = require('chai').expect;

describe('TOP-LEVEL-DESCRIPTION', () => {
    let browser;
    let page;

    before(async () => {
        browser = await puppeteer.launch({
            headless: false,
            slowMo: 0,
            devtools: false,
        });
    });

    beforeEach(async () => {
        page = await browser.newPage();
        await page.goto('SOME-URL');
    });

    after(async () => {
        await browser.close();
    });

    it('TEST-CASE', () => {
        //
    });
});
